export const screen = {
    mobile: "(max-width: 500px)",
    tablet: "(max-width: 768px)",
    laptop: "(max-width: 1024px)",
    LaptopL: "(max-width: 1200px)",
    desktop: "(min-width: 1200px)"
}