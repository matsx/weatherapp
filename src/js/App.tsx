import React from 'react';
import AboutApp from "./AboutApp";
import Header from "./Header";
import WeatherMap from "./WeatherMap";
import CurrentWeather from "./CurrentWeather"
import { BrowserRouter as Router, Route, Switch, Link } from "react-router-dom";

const App= () => {
    console.log(process.env.PUBLIC_URL)
    return (
    <>
        <Router>
            <Switch>
                <Route exact path="/weatherapp">
                    <Header/>
                    <AboutApp/>
                </Route>
                <Route path="/weatherapp/map" component={WeatherMap}>
                   
                </Route>
            </Switch>
        </Router>
    </>
    )
}

export default App;