import React, {useEffect, useState} from 'react';
import Map from 'ol/Map';
import View from 'ol/View';
import TileLayer from 'ol/layer/Tile';
import OSM from 'ol/source/OSM';
import styled from "styled-components"
import Point from 'ol/geom/Point';
import Cities from "../data/pl.json"
import Collection from 'ol/Collection';
import VectorLayer from 'ol/layer/Vector';
import VectorSource from 'ol/source/Vector';
import Feature from 'ol/Feature';
import {useGeographic as geographic} from 'ol/proj';
import axios from 'axios'
import {Fill, Stroke, Circle, Style} from 'ol/style';
import Icon from 'ol/style/Icon';
import Text from 'ol/style/Text';

import rainy from "../icons/static/rainy-4.svg";
import rainyNight from "../icons/static/cloudy-night-3.svg";
import clearNight from "../icons/static/night.svg";
import clear from "../icons/static/day.svg";


const StyledDiv=styled.div`
    position: absolute;
    height:100%;
    width: 100%; 
`

type station= {
  id_stacji: string
  stacja: string
  data_pomiaru: string
  godzina_pomiaru: string
  temperatura: string
  predkosc_wiatru: string
  kierunek_wiatru: string
  wilgotnosc_wzgledna: string
  suma_opadu: string
  cisnienie: string
}

const WeatherMap= () => {
  const [stations, setStationsData]= useState();
  const [zoom, setZoomValue]= useState(6)
  const [olMap, setOlMap]= useState();

  const returnRightIcon:any = (station:station) => {
    if (Number(station.suma_opadu) > 3 && 
      (Number(station.godzina_pomiaru) >= 21 || 
      Number(station.godzina_pomiaru) <= 6 )){
        return rainyNight}
    else if(Number(station.suma_opadu) > 3 && 
    (Number(station.godzina_pomiaru) >= 21 || 
    Number(station.godzina_pomiaru) <= 6 )) {
        return clearNight
    }
    else if (Number(station.suma_opadu) > 3) {
      return rainy
    }
    else {
      return clear
    }
  }

    useEffect(()=> {
      geographic();
      if (!olMap) {
      let map:any = new Map({
        view: new View({
          center: [19, 52.5],
          zoom: zoom
        }),
        layers: [
          new TileLayer({
            source: new OSM()
          })
        ],
        target: 'map'
      });
      setOlMap(map);
      map.getView().on('change:resolution', (event:any) => {
        setZoomValue(event.target.getZoom());
      });
      }
    })

    useEffect(()=>{
        if(typeof olMap != 'undefined' && stations) {
        olMap.removeLayer(olMap.getLayers().getArray()[1])
        const citiesColection:Collection<Feature>= new Collection()
        Cities.forEach((el)=> {
          stations.data.forEach((station:station, index:number)=> {
            if(station.stacja === el.city){
              let dataQuantity: number
              zoom > 7 ? dataQuantity=1 : dataQuantity=4
              if (index % dataQuantity === 0) {
              const coords:number[]= [Number(el.lng), Number(el.lat)]
              const point= new Point(coords)
              const city:Feature= new Feature(point);
              const temp= new Text({
                text: `${parseInt(station.temperatura)}`,
                scale: 1.33,
                offsetY: 20,
                offsetX: -12,
                font: "bold 16px serif"

              })
              const style= new Style({
                image: new Icon({
                  src: returnRightIcon(station)
                }),
                text: temp
              })
              city.setStyle(style)
              city.set("temp", station.temperatura);
              city.set("fall", station.suma_opadu);
              citiesColection.push(city);
              }  
            }
          })
        })
        const vectorSource= new VectorSource({
            features: citiesColection
        })
        var layer:VectorLayer= new VectorLayer({
            source: vectorSource
        })
        olMap.addLayer(layer);
      }  
    })
    
    useEffect(()=>{
      if(!stations) {
      axios.get("https://danepubliczne.imgw.pl/api/data/synop/").then(resp => {
        setStationsData(resp)  
      })
    }
    })

    return (
        <StyledDiv id="map">
        </StyledDiv>
    );
}

export default WeatherMap;