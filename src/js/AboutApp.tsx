import React, {useState, useEffect} from 'react';
import styled from "styled-components"
import { Transition } from 'react-transition-group';
import { Link } from "react-router-dom";
import CurrentWeather from "./CurrentWeather";
import {screen} from "./mediaSizes";




const StyledDiv= styled.div`
    background-color: #0C1821;
    z-index: -1;
`;

const StyledHeader= styled.h1`
    height: 30px;
    grid-column-start: 2;
    grid-column-end: 6;
    grid-row-start: 2;
    grid-row-end: 2;
    text-align: center;
    margin-left: -5%;
    @media ${screen.laptop} {
        grid-column-start: 1;
        grid-column-end: 11;
        grid-row-start: 2;
        grid-row-end: 2;
        text-align: center;
    }
`;

const StyledP= styled.p`
    grid-column-start: 2;
    grid-column-end: 6;
    grid-row-start: 3;
    grid-row-end: 3;
    font-size: 14px;
    @media ${screen.laptop} {
        grid-column-start: 1;
        grid-column-end: 11;
        grid-row-start: 7;
        grid-row-end: 7;
        text-align: center;
    }
    `

    const StyledIMGWP= styled.p`
    grid-column-start: 2;
    grid-column-end: 6;
    grid-row-start: 5;
    grid-row-end: 5;
    margin-top: 50px;
    font-size: 14px;
    @media ${screen.laptop} {
        grid-column-start: 1;
        grid-column-end: 11;
        grid-row-start: 9;
        grid-row-end: 9;
        text-align: center;
        padding: 5px;
    }
    `


const StyledButton= styled.button`
    width: 200px;
    height: 50px;
    background: transparent;
    border-radius: 2px;
    border: 2px solid white;
    color: white;
    cursor: pointer;
    @media ${screen.laptop} {
        grid-column-start: 5;
        grid-column-end: 5;
        grid-row-start: 8;
        grid-row-end: 8;
        text-align: center;
    }
    &:hover{
        border-color: #324A5F;
        color: #41617d;
        transition-delay: 0.55s;
    }
`;

const duration = 600;

const defaultStyle = {
  transition: `opacity ${duration}ms ease-in-out`,
  opacity: 0,
  
}

const defaultStyleLeft= {
    transition: `all ${duration}ms ease-in-out`,
    opacity: 0,
    transform: 'translate(-100%)'
}

const transitionStylesLeft: any = {
    entering: { opacity: 0,  transform: 'translate(-100%)'},
    entered:  { opacity: 1, transform: 'translate(0%)'},
    exiting:  { opacity: 1, transform: 'translate(0%)'},
    exited:  { opacity: 0, transform: 'translate(-100%)'},
  };

  const defaultStyleRight= {
    transition: `all ${duration}ms ease-in-out`,
    opacity: 0,
    transform: 'translate(100%)'
}

const transitionStylesBottom: any = {
    entering: { opacity: 0,  transform: 'translateY(300%)'},
    entered:  { opacity: 1, transform: 'translate(0%)'},
    exiting:  { opacity: 1, transform: 'translate(0%)'},
    exited:  { opacity: 0, transform: 'translateY(300%)'},
  };

  const defaultStyleBottom= {
    transition: `all ${duration}ms ease-in-out`,
    opacity: 0,
    transform: 'translate(300%)'
}

const transitionStylesRight: any = {
    entering: { opacity: 0,  transform: 'translate(100%)'},
    entered:  { opacity: 1, transform: 'translate(0%)'},
    exiting:  { opacity: 1, transform: 'translate(0%)'},
    exited:  { opacity: 0, transform: 'translate(100%)'},
  };

const transitionStyles: any = {
  entering: { opacity: 0 , },
  entered:  { opacity: 1 },
  exiting:  { opacity: 1 },
  exited:  { opacity: 0 },
};

const AboutApp= () => {
    const [animationStart, changeAnimationState]= useState(false);
    useEffect(()=> {
        if (!animationStart){
            changeAnimationState(true)
        }
    })

    return(
        <>
            <StyledDiv className="container">
                        <Transition in={animationStart} timeout={500}>
                            {state =>(
                        <StyledHeader style={{
                            ...defaultStyle,
                            ...transitionStyles[state]
                        }}
                        data-testid='header'>Sprawdź aktualną pogodę</StyledHeader>
                        )}
                        </Transition>
                        <Transition in={animationStart} timeout={800}>
                        {state =>(
                        <StyledP style={{...defaultStyleLeft, ...transitionStylesLeft[state]}}>Przeglądaj aktualne dane pogodowe z 60
                        stacji pogodowych z całej Polski. Informacje ze stacji pomiarowych obejmują: <br/>
                        - ciśnienie <br/>
                        - temperaturę <br/>
                        - opady <br/>
                        - wilgostność powietrza

                        </StyledP>
                        )}
                        </Transition>
                        <Transition in={animationStart} timeout={1100}>
                        {state =>(
                        <StyledIMGWP style={{...defaultStyleRight, ...transitionStylesRight[state]}}>
                            Dane udostępniane są przez Instytut Meteorologii i Gospodarki Wodnej. 
                            Informacje aktualizowane są co godzinę. Udostępniane są pod adresem:   
                            <a href="https://danepubliczne.imgw.pl/apiinfo">
                                 https://danepubliczne.imgw.pl/apiinfo</a>
                        </StyledIMGWP>
                        )}
                        </Transition>
                        <Transition in={animationStart} timeout={1400}>
                            {state =>(
                            
                            <Link to="/weatherapp/map" className="linkToMap">
                                <StyledButton className="openMap" style={{...defaultStyleBottom, ...transitionStylesBottom[state]}}>Sprawdź teraz</StyledButton>
                            </Link>
                            )}
                        </Transition>
                        <CurrentWeather/>
            </StyledDiv>
            
            </>
                
    )
}

export default AboutApp;