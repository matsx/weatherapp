import React, {useState, useEffect} from 'react';
import styled from "styled-components";
import { Transition } from 'react-transition-group';

const StyledDiv= styled.div`
    position: fixed;
    width: 100%;
    height: 80px;
    color: white;
    background-color: #0C1821;
    border-bottom: 3px solid #324A5F; 
    top: 0px
`;

const StyledSpan= styled.span`
    font-size: 25px;
    font-family: 'Poppins', sans-serif;
`;

const StyledTitleContainer= styled.div`
    width: 40%;
    left: 30%;
    text-align: center;
    position: relative;
    height: 100%;
    line-height: 450%;
`;

const defaultStyle = {
    transition: `opacity ${330}ms ease-in-out`,
    opacity: 0,
  }

const transitionStyles: any = {
entering: { opacity: 0 , },
entered:  { opacity: 1 },
exiting:  { opacity: 1 },
exited:  { opacity: 0 },
};

const defaultTranslationStyle = {
    transition: `transform ${450}ms ease-in-out`,
  }

const translationStyles:any={
    entering: { transform:  'translate(0%)' },
    entered:  { transform:  window.innerWidth < 600 ? 'translate(-60%)' : 'translate(-90%)' },
    exiting:  { transform:  window.innerWidth < 600 ? 'translate(-60%)' : 'translate(-90%)' },
    exited:  { transform:  window.innerWidth < 600 ? 'translate(-60%)' : 'translate(-90%)' },
}


const Header= () => {
    const [animationStart, setAnimationStart]= useState(false);

    useEffect(()=>{
        if(!animationStart){
            setAnimationStart(true)
        }
    })

    return (
        <StyledDiv>
            <Transition in={animationStart} timeout={2600}>
            {state=>(
            <StyledTitleContainer  style={{
                ...defaultTranslationStyle,
                ...translationStyles[state]
            }}>
                <Transition in={animationStart} timeout={200}>
                    {state =>(
                    <StyledSpan style={{
                        ...defaultStyle,
                        ...transitionStyles[state]}}>W</StyledSpan>
                    )}
                </Transition>
                <Transition in={animationStart} timeout={400}>
                    {state =>(
                    <StyledSpan style={{
                        ...defaultStyle,
                        ...transitionStyles[state]}}>e</StyledSpan>
                    )}
                </Transition>
                <Transition in={animationStart} timeout={600}>
                    {state =>(
                    <StyledSpan style={{
                        ...defaultStyle,
                        ...transitionStyles[state]}}>a</StyledSpan>
                    )}
                </Transition>
                <Transition in={animationStart} timeout={800}>
                    {state =>(
                    <StyledSpan style={{
                        ...defaultStyle,
                        ...transitionStyles[state]}}>t</StyledSpan>
                    )}
                </Transition>
                <Transition in={animationStart} timeout={1000}>
                    {state =>(
                    <StyledSpan style={{
                        ...defaultStyle,
                        ...transitionStyles[state]}}>h</StyledSpan>
                    )}
                </Transition>
                <Transition in={animationStart} timeout={1200}>
                    {state =>(
                    <StyledSpan style={{
                        ...defaultStyle,
                        ...transitionStyles[state]}}>e</StyledSpan>
                    )}
                </Transition>
                <Transition in={animationStart} timeout={1400}>
                    {state =>(
                    <StyledSpan style={{
                        ...defaultStyle,
                        ...transitionStyles[state]}}>r</StyledSpan>
                    )}
                </Transition>
                <Transition in={animationStart} timeout={1600}>
                    {state =>(
                    <StyledSpan style={{
                        ...defaultStyle,
                        ...transitionStyles[state]}}>_</StyledSpan>
                    )}
                </Transition>
                <Transition in={animationStart} timeout={1800}>
                    {state =>(
                    <StyledSpan style={{
                        ...defaultStyle,
                        ...transitionStyles[state]}}>a</StyledSpan>
                    )}
                </Transition>
                <Transition in={animationStart} timeout={2000}>
                    {state =>(
                    <StyledSpan style={{
                        ...defaultStyle,
                        ...transitionStyles[state]}}>p</StyledSpan>
                    )}
                </Transition>
                <Transition in={animationStart} timeout={2200}>
                    {state =>(
                    <StyledSpan style={{
                        ...defaultStyle,
                        ...transitionStyles[state]}}>p</StyledSpan>
                    )}
                </Transition>    
            </StyledTitleContainer>
            )}
            </Transition>
        </StyledDiv>
    );
}

export default Header;