import React, {useState, useEffect} from 'react';
import styled, {keyframes} from 'styled-components';
import axios from "axios";
import {screen} from "./mediaSizes"

import rainy from "../icons/static/rainy-4.svg";
import rainyNight from "../icons/static/cloudy-night-3.svg";
import clearNight from "../icons/static/night.svg";
import clear from "../icons/static/day.svg";

type station= {
    id_stacji: string | null
    stacja: string | null
    data_pomiaru: string | null
    godzina_pomiaru: string | null
    temperatura: string | null
    predkosc_wiatru: string | null
    kierunek_wiatru: string | null
    wilgotnosc_wzgledna: string | null
    suma_opadu: string | null
    cisnienie: string | null
  }

const horizontalMove= keyframes`
    from {
        left: 0px;
    }
    100% { 
        left: -15500px;
    }
    }
`

const StyledDiv= styled.div`
    grid-column-start: 1;
    grid-column-end: 11;
    grid-row-start: 7;
    grid-row-end: 12;
    @media ${screen.laptop} {
        grid-column-start: 1;
        grid-column-end: 11;
        grid-row-start: 3;
        grid-row-end: 6;
        text-align: center;
    }
    @media ${screen.mobile} {
        grid-column-start: 1;
        grid-column-end: 11;
        grid-row-start: 3;
        grid-row-end: 5;
        text-align: center;
    }
}
`;

const FlowContainer= styled.div`
    width: 15500px;
    position: relative;
    animation: ${horizontalMove} 300s linear infinite;
    height: 100%;
`;

const StyledWeatherInfo= styled.div`
    display: inline-block;
    width: 230px;
    margin-left: 5px;
    border: 1px solid #324A5F;
    height: 90%
`
const StyledTable= styled.table``;

const StyledTdFeature= styled.td``;


const CurrentWeather = () => {
    const [weatherData, setWeatherData]= useState({
        data: []
    })
    const noData= "---";
    useEffect(()=>{
        if (!weatherData.data.length){
            axios.get("https://danepubliczne.imgw.pl/api/data/synop/").then(resp => {
                if(resp) {
                    setWeatherData(resp) 
                }
            }).catch((err) => 
            console.error(err))
        }
    })

    const renderCurrentWeather:any = () =>{
        let weatherDataElements:any[]=[];
        if(weatherData.data.length) {
            weatherDataElements= weatherData.data.map((el:station)=>(
                <StyledWeatherInfo>
                    {Number(el.suma_opadu) > 3 ? <img src={rainy}></img> : <img src={clear}></img>}
                    <StyledTable>
                        <tbody>
                        <tr>
                            <StyledTdFeature>Stacja: </StyledTdFeature>
                            <td>{el.stacja ? el.stacja : noData}</td>
                        </tr>
                        <tr>
                            <StyledTdFeature>Temperatura: </StyledTdFeature>
                            <td>{el.temperatura ? el.temperatura : noData}</td>
                        </tr>
                        <tr>
                            <td>Ciśnienie: </td>
                            <td>{el.cisnienie ? el.cisnienie : noData}hPa</td>
                        </tr>
                        <tr>
                            <StyledTdFeature>Wilgotność: </StyledTdFeature>
                            <td>{el.wilgotnosc_wzgledna ? el.wilgotnosc_wzgledna : noData}</td>
                        </tr>
                        <tr>
                            <StyledTdFeature>Opad: </StyledTdFeature>
                            <td>{el.suma_opadu ? el.suma_opadu : noData}mm</td>
                        </tr>
                        </tbody>
                    </StyledTable>

                </StyledWeatherInfo>
            ))
        }
        else {
            weatherDataElements= [];
        }
        return weatherDataElements
    }

    return(<StyledDiv>
        <FlowContainer>
        {renderCurrentWeather()}
        </FlowContainer>
    </StyledDiv>)
}

export default CurrentWeather;