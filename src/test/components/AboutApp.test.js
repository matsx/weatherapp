
import AboutApp from "../../js/AboutApp"
import React from 'react';
import ReactDOM from "react-dom"
import { BrowserRouter as Router, Route, Switch, Link } from "react-router-dom";
import {render} from "@testing-library/react"
import "@testing-library/jest-dom/extend-expect";

describe('AboutApp', ()=> {
    it('component renders', ()=>{
       const div= document.createElement('div');
       ReactDOM.render(<Router><AboutApp/></Router>, div)
    })
})

describe('AboutApp2', ()=> {
    it('component renders header with specific text', ()=> {
    const {getByTestId}= render(<Router><AboutApp></AboutApp></Router>)
    expect(getByTestId('header')).toHaveTextContent('Sprawdź aktualną pogodę')

})
})

